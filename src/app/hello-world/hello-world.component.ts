import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {

  name: string;
  customer: Customer;
  cssClass: string;

  ngOnInit() {
  }

  constructor() {
    this.name = 'my name';

    this.customer = {
      name: 'chris',
      age: 12,
      gender: 'm'
    }
  }

  get isMale() {
    return this.customer.gender === 'm';
  }

  changeGender(param: string) {
    this.customer.gender = param;
    this.cssClass = (this.customer.gender === 'f')
      ? 'female' : 'male';
  }

  alert(param: string) {
    window.alert(param);
  }
}

interface Customer {
  age: number;
  name: string;
  gender: string;
}