import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // assuming we get this list from API
  menuList;
  todoList;
  menuTitle = 'my MENU';
  count = 0;
  // inject todo service in constructor

  constructor(private todoSvc: TodoService) {
  }

  ngOnInit() {
    // after i call API and get the value
    // something in service
    this.todoList = this.todoSvc.getList();

    this.menuList = [
      { name: 'home', url: '/home' },
      { name: 'about', url: '/about' },
      { name: 'google', url: 'www.google.com' },
    ];
  }

  triggerLoginCount(num: number) {
    this.count = this.count + num;
  }
}


