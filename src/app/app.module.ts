import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './hello-world/hello-world.component';
import { MenuListComponent } from './menu-list/menu-list.component';

import { TodoService } from './todo.service';

@NgModule({
  declarations: [
    AppComponent,
    HelloWorldComponent,
    MenuListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule // add this line
  ],
  providers: [
    // { provide: 'TodoService', useClass: TodoService }
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
