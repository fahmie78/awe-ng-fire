import { Injectable } from '@angular/core';

@Injectable()
export class TodoService {

  constructor() { }

  getList(): TodoItem[] {
    // later on, i will call an API
    // to get data from my database
    return [
      { desc: 'read book' },
      { desc: 'play badminton', isCompleted: true },
      { desc: 'go sleeping' }
    ]
  }
}

interface TodoItem {
  desc: string;
  isCompleted?: boolean;
}
